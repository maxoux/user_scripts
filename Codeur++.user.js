// ==UserScript==
// @name         Codeur++
// @namespace    http://devfoundry.fr/
// @version      0.5
// @description  Auto-filler + indicators
// @author       Matthieu Laizé
// @match        https://www.codeur.com/projects/*
// @match        https://www.codeur.com/users/*/messages*
// @require      https://raw.githubusercontent.com/lodash/lodash/4.17.4/dist/lodash.core.js
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_xmlhttpRequest
// @grant        none
// ==/UserScript==

/**
 *
 *    Constantes
 *
 */

const match_dict = {
    "https://www.codeur.com/projects/b/*": project_list_upgrade,
    "https://www.codeur.com/projects/*": project_upgrade,
    "https://www.codeur.com/users/*/messages": messages_upgrade
};

const offer_amount = 400;
const offer_time = 60;
const keyword_template = "<Project>";

const template_public = `Bonjour,

Votre annonce a attiré notre attention.

Nous sommes une jeune agence de développement polyvalente située à Paris.

Nous vous avons envoyé une proposition concernant cette offre en message privé.

Nous venons d’horizons différents et nous sommes réunis dans un objectif commun lors de notre formation à l’école 42 (école fondée par le PDG de Free, similaire à Epitech), dans le but de pouvoir réaliser ensemble des projets pour des clients avec la meilleure qualité possible.

Nous pouvons réaliser tous types de projets techniques (mobile, web, design, back-end et/ou front-end) et accompagner nos clients durant toute la durée de vie de leurs projets.

Cordialement,

L’équipe DevFoundry`;

const template_private = `Bonjour,

Votre ${keyword_template} nous intéresse. Nous souhaitons vous accompagner pour le réaliser.

Nous sommes une équipe de jeunes développeurs située à Paris, passionnés de nouvelles technologies et aux compétences complémentaires. Notre point commun ? Nous avons été formés au développement informatique à l'école 42, fondée par le PDG de Free. Nous vous proposons d'apporter l'expertise qui fera la différence dans votre projet.

Pour répondre au mieux à vos besoins et les estimer précisément, nous aurions besoin idéalement des documents suivants : 
- Les cahiers des charges détaillés afin de prendre en compte toutes vos fonctionnalités. 
- Les maquettes d’interfaces afin d'en évaluer la complexité. 
- Toute autre pièce ou information qui pourraient nous aider à réaliser votre vision.

Si notre équipe vous intéresse, n'hésitez pas à me contacter via cette plateforme ou par mail à l'adresse ci-dessous.

Cordialement,

Marc-Olivier MASCIO 
m.mascio@devfoundry.fr 
https://devfoundry.fr`;

/**
 *
 *    Boite à outils
 *
 */

function wildCompareNew(string, search) {
    var startIndex = 0, array = search.split('*');
    for (var i = 0; i < array.length; i++) {
        var index = string.indexOf(array[i], startIndex);
        if (index == -1) return false;
        else startIndex = index;
    }
    return true;
}

/**
 *
 *    Core
 *
 */

function project_upgrade() {
     // Elements importants
    var infos =  $(".infos").find("tbody");

    // Nombre d'offres
    var nb_offers = $(".offers").find(".list").children().length;
    var offers_row = `<tr>
        <td class="muted">Nb d'offres</td>
        <td>${nb_offers}</td>
        </tr>`;

    // Nombre de réponses
    var nb_response = $(".fa-comment-o").length;

    var response_row = `<tr>
                          <td class="muted">Dont réponse</td>
                          <td>${nb_response}</td>
                       </tr>`;

    // Vérification du pays
    var flag = $(".user-infos").find(".user-localisation");
    var isFrench = flag.find("img").attr("src").indexOf("FR-") != -1;

    // Affichage
    infos.prepend(response_row);
    infos.prepend(offers_row);
    if (!isFrench)
        flag.css("background-color", "red");

    // Création d'offre
    $(".offer_button").find("a").click(function(){
        setTimeout(function(){
            $("#offer_amount").val(offer_amount);
            $("#offer_duration").val(offer_time);
            $("#offer_description").val(template_public);
            $("#offer_comments_attributes_0_content").val(template_private);
            $("#offer_comments_attributes_0_content").css("height", 700);
            $("#offer_accept_tasks_payments").prop("checked", !isFrench);
        }, 200);
    });
}

function messages_upgrade() {

    // Récupération des informations de topics
    var messages = _.map($(".message"), function(item){
        let info = $(item).find(".username")[0].innerText.split(" ");
        info.pop();
        let messages = info.pop();
        let username = info.join(' ');
        let url = $(item).find(".name").find("a").attr("href");

        return {div: item,
                username: username,
                nb_messages: messages,
                url: url,
                unread: $(item).hasClass("unread")
               };
    });

    console.log(`parsing ${messages.length} messages`);

    // Execution des améliorations
    messages.forEach(function(item){
        let cached = JSON.parse(localStorage.getItem(item.url));
        let nb_messages_cached = cached ? cached.nb_messages : -1;

        if (!item.unread && item.nb_messages != 1 && nb_messages_cached != item.nb_messages) {
            $.ajax(item.url)
             .done(function(data){

                var html = $.parseHTML(data);
                var list = $(html).find(".comment").map(function(i, chat){
                    return chat.className.split(" ")[1];
                });
                last_comment = list[list.length - 1];
                console.log(`Fetched : ${item.url}`);

                if (last_comment == "me")
                    $(item.div).css("background-color", "cyan");

                localStorage.setItem(item.url, JSON.stringify({nb_messages: item.nb_messages, responded: last_comment == "me"}));
             });
        }
        else if (cached) {
            if (!cached.responded)
                $(item.div).css("background-color", "cyan");
            console.log(`Cached : ${item.url}`);
        }
        else
            console.log(`Ignored : ${item.url}`);

    });
}

function project_list_upgrade() {
    var items = _.map($(".small-project"), function(item){
        return {
            div: item,
            username: $(item).find(".author-link")[0].innerText,
            url: $(item).find(".author-link").attr("href") + "/projets-crees",
            name: $(item).find("a")[0].innerText,
        };
    });
    console.log('items : ', items);

    items.forEach(function(item){
        $.ajax(item.url)
         .done(function(data){

            var html = $.parseHTML(data);
            var has_project_offered = $(html).find(".label-offered").length;
            console.log(`Parsing offer [${item.name}] : ${has_project_offered}`);

            if (has_project_offered && $(item.div).find(".label-offered").length != 1)
                $(item.div).css("background-color", "yellow");
         });
    });
}

(function() {
    'use strict';

   var url = document.location.href.split("?")[0];
   var checked = false;
    
   for (var path in match_dict) {
       if (wildCompareNew(url, path) && !checked) {
           match_dict[path]();
           checked = true;
       }
   }

})();